﻿using JobAssigment.Models;
using JobAssigment.ViewModels.Home;
using System.Collections.Generic;


namespace JobAssigment.Repository
{
    public interface IJobRepository
    {
        public void Add(Job newJob);

        public void Remove(int id);

        public Job GetByID(int id);

        public IEnumerable<Job> GetAll(JobStatus jobstatus);

        public void Update(Job job);

    }
}
