﻿using JobAssigment.Data;
using JobAssigment.Models;
using JobAssigment.ViewModels.Home;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JobAssigment.Repository
{
    public class JobRepository : IJobRepository
    {
        private readonly ApplicationDbContext appDbContext;

        public JobRepository(ApplicationDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public void Add(Job newJob)
        {
            appDbContext.Jobs.Add(newJob);
            appDbContext.SaveChanges();
        }

        public IEnumerable<Job> GetAll(JobStatus jobstatus)
        {
            switch (jobstatus)
            {
                case JobStatus.Accepted:
                    return appDbContext.Jobs.Where(x => x.IsAcceptedTheJob.HasValue ? x.IsAcceptedTheJob.Value == true : false);
                case JobStatus.Declined:
                    return appDbContext.Jobs.Where(x => x.IsAcceptedTheJob.HasValue ? x.IsAcceptedTheJob.Value == false : false);
                case JobStatus.Pending:
                    return appDbContext.Jobs.Where(x => !x.IsAcceptedTheJob.HasValue);

                default:
                    return appDbContext.Jobs;
            };
        }



        public Job GetByID(int id)
        {
            return appDbContext.Jobs.FirstOrDefault(x => x.Id == id);
        }

        public void Remove(int id)
        {
            appDbContext.Remove(GetByID(id));
            appDbContext.SaveChanges();
        }

        public void Update(Job company)
        {
            appDbContext.Update(company);
            appDbContext.SaveChanges();
        }
    }
}
