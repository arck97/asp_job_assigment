﻿using JobAssigment.Models;
using System.Collections.Generic;

namespace JobAssigment.Repository
{
    public interface ICompanyRepository
    {
        public void Add(Company newCompany);

        public void Remove(int id);

        public Company GetByID(int id);

        public IEnumerable<Company> GetAll();

        public void Update(Company company);


    }
}
