﻿using JobAssigment.Data;
using JobAssigment.Models;
using System.Collections.Generic;
using System.Linq;

namespace JobAssigment.Repository
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly ApplicationDbContext appDbContext;

        public CompanyRepository(ApplicationDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public void Add(Company newCompany)
        {
            appDbContext.Companies.Add(newCompany);
            appDbContext.SaveChanges();
        }

        public IEnumerable<Company> GetAll()
        {
            return appDbContext.Companies;
        }

        public Company GetByID(int id)
        {
           return appDbContext.Companies.FirstOrDefault(x => x.Id == id);
        }

        public void Remove(int id)
        {
            appDbContext.Remove(GetByID(id));
        }

        public void Update(Company company)
        {
            appDbContext.Update(company);
        }
    }
}
