﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using JobAssigment.Models;
using Microsoft.AspNetCore.Identity;
using JobAssigment.Data;
using JobAssigment.Repository;
using JobAssigment.ViewModels.Home;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Collections;
using System.Collections.Generic;

namespace JobAssigment.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        RoleManager<IdentityRole> rolemanager; //szerepkörkezelő
        EmployeeUserManager usermanager; //felhasználókezelő
        IJobRepository jobRepository;

        public HomeController(RoleManager<IdentityRole> rolemanager,
           EmployeeUserManager usermanager, IJobRepository jobRepository)
        {
            this.usermanager = usermanager;
            this.rolemanager = rolemanager;
            this.jobRepository = jobRepository;
        }

        [HttpGet]
        public async Task<IActionResult> FirstStep()
        {
            //1. csinálunk egy admin szerepkört
            //2. beletesszük az első user-t

            IdentityRole adminrole = new IdentityRole()
            {
                Name = "admin"
            };
            await rolemanager.CreateAsync(adminrole);

            var firstuser = usermanager.Users.FirstOrDefault();

            await usermanager.AddToRoleAsync(firstuser, "admin");

            return RedirectToAction("Index");
        }

        public IActionResult Index()
        {
            bool isAdmin = User.IsInRole("admin");

            IndexViewModel model = new IndexViewModel();
            model.IsAdmin = isAdmin;

            IEnumerable<Job> filteredJobs = GetFilteredJobs(model.SelectedStatus, isAdmin);
            foreach (var item in filteredJobs)
            {
                model.IndexListItems.Add(MapJobEntityToIndexListViewModel(item));
            }

            return View("~/Views/Home/Index.cshtml", model);
        }

        [HttpPost]
        public IActionResult Filter(IndexViewModel model)
        {
            bool isAdmin = User.IsInRole("admin");

            IEnumerable<Job> filteredJobs = GetFilteredJobs(model.SelectedStatus, isAdmin);
            foreach (var item in filteredJobs)
            {
                model.IndexListItems.Add(MapJobEntityToIndexListViewModel(item));
            }

            return View("~/Views/Home/Index.cshtml", model);
        }

        public IActionResult Privacy()
        {
            return View("~/Views/Home/Privacy.cshtml");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        private IEnumerable<Job> GetFilteredJobs(JobStatus jobStatus, bool isAdmin)
        {
            if (isAdmin)
            {
                return jobRepository.GetAll(jobStatus);
            }
            else
            {
                return jobRepository.GetAll(jobStatus)
                                    .Where(x => x.AssignedEmployeeId == usermanager.GetUserAsync(User)
                                                                                   .GetAwaiter()
                                                                                   .GetResult().Id);
            }
        }

        private IndexListItemViewModel MapJobEntityToIndexListViewModel(Job jobEntity)
        {
            return new IndexListItemViewModel
            {
                Id = jobEntity.Id,
                Name = jobEntity.Name,
                OwnerName = jobEntity.Owner?.UserName,
                AssignedEmployeeName = jobEntity.AssignedEmployee?.UserName,
                IsAcceptedTheJob = jobEntity.IsAcceptedTheJob,
                DeadLine = jobEntity.DeadLine
            };
        }
    }
}
