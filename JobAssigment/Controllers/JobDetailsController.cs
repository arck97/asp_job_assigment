﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobAssigment.Repository;
using JobAssigment.ViewModels.Home;
using JobAssigment.ViewModels.JobDetails;
using Microsoft.AspNetCore.Mvc;

namespace JobAssigment.Controllers
{
    public class JobDetailsController : Controller
    {
        private readonly IJobRepository _jobRepository;

        public JobDetailsController(IJobRepository jobRepository)
        {
            _jobRepository = jobRepository;
        }

        public IActionResult Index(int id)
        {
            var jobEntity = _jobRepository.GetByID(id);

            DetailsViewModel model = new DetailsViewModel
            {
                Id = id,
                AssignedEmployeeName = jobEntity.AssignedEmployee?.UserName,
                OwnerName = jobEntity.Owner?.UserName,
                Description = jobEntity.Description
            };

            return View("~/Views/JobDetails/Index.cshtml", model);
        }

        [HttpPost]
        public IActionResult AddDescription(DetailsViewModel model)
        {
            if (!ModelState.IsValid)
                return View("~/Views/JobDetails/Index.cshtml", model);

            var jobEntity = _jobRepository.GetByID(model.Id);
            jobEntity.Description = model.Description;
            _jobRepository.Update(jobEntity);

            return RedirectToAction("Index", "Home");
        }
    }
}