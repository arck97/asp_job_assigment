﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobAssigment.Data;
using JobAssigment.Models;
using JobAssigment.Repository;
using JobAssigment.ViewModels;
using JobAssigment.ViewModels.Home;
using JobAssigment.ViewModels.Job;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace JobAssigment.Controllers
{
    [Authorize]
    public class JobController : Controller
    {

        private readonly EmployeeUserManager _userManager;
        private readonly IJobRepository _jobRepository;

        public JobController(EmployeeUserManager userManager, IJobRepository jobRepository)
        {
            _userManager = userManager;
            _jobRepository = jobRepository;
        }

        [HttpGet]
        public IActionResult Create()
        {
            CreateJobViewModel model = new CreateJobViewModel()
            {
                DeadLine = DateTime.Now,
                StartTheJob = DateTime.Now,
                EndTheJob = DateTime.Now,
                AvailableEmployees = GetUsersForDropdown()
            };

            return View("~/Views/Job/CreateJob.cshtml", model);
        }

        [HttpPost]
        public IActionResult Create(CreateJobViewModel model)
        {
            if (ModelState.IsValid)
            {
                _jobRepository.Add(new Job
                {
                    Name = model.JobName,
                    StartTheJob = model.StartTheJob,
                    EndTheJob = model.EndTheJob,
                    DeadLine = model.DeadLine,
                    AssignedEmployeeId = model.AssignedToEmployeeId,
                    OwnerId = _userManager.GetUserId(User),
                    IsAcceptedTheJob = null
                });

                return RedirectToAction("Index", "Home");
            }

            model.AvailableEmployees = GetUsersForDropdown();
            return View("~/Views/Job/CreateJob.cshtml", model);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _jobRepository.Remove(id);
            return RedirectToAction("Index", "Home");


        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var job = _jobRepository.GetByID(id);
            EditViewModel model = new EditViewModel
            {
                Id = id,
                StartTheJob = job.StartTheJob,
                EndTheJob = job.EndTheJob,
                DeadLine = job.DeadLine,
                JobName = job.Name,
                AssignedToEmployeeId = job.AssignedEmployeeId,
                AvailableEmployees = GetUsersForDropdown()
            };

            return View("~/Views/Job/Edit.cshtml", model);
        }

        [HttpPost]
        public IActionResult Edit(EditViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.AvailableEmployees = GetUsersForDropdown();
                return View("~/Views/Job/Edit.cshtml", model);
            }
            var jobEntity = _jobRepository.GetByID(model.Id);
            jobEntity.Name = model.JobName;
            jobEntity.StartTheJob = model.StartTheJob;
            jobEntity.EndTheJob = model.EndTheJob;
            jobEntity.DeadLine = model.DeadLine;
            jobEntity.AssignedEmployeeId = model.AssignedToEmployeeId;

            _jobRepository.Update(jobEntity);

            return RedirectToAction("Index", "Home");
        }

        public IActionResult DeclineJob(int id)
        {
            var jobEntity = _jobRepository.GetByID(id);
            jobEntity.IsAcceptedTheJob = false;
            _jobRepository.Update(jobEntity);

            return RedirectToAction("Index", "Home");
        }

        public IActionResult AcceptJob(int id)
        {
            var jobEntity = _jobRepository.GetByID(id);
            jobEntity.IsAcceptedTheJob = true;
            _jobRepository.Update(jobEntity);

            return RedirectToAction("Index", "Home");
        }

        private List<SelectListItem> GetUsersForDropdown()
        {
            return _userManager.Users.ToList().Where(x => !(_userManager.IsInRoleAsync(x, "admin")
                                                                          .GetAwaiter()
                                                                          .GetResult()))
                                                                          .Select(x => new SelectListItem
                                                                          {
                                                                              Text = x.UserName,
                                                                              Value = x.Id
                                                                          }).ToList();
        }
    }
}