﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JobAssigment.ViewModels.JobDetails
{
    public class DetailsViewModel
    {
        public int Id { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        public string OwnerName { get; set; }

        public string AssignedEmployeeName { get; set; }
    }
}
