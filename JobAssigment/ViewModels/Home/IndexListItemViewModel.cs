﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobAssigment.ViewModels.Home
{
    public class IndexListItemViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool? IsAcceptedTheJob { get; set; }

        public string OwnerName { get; set; }

        public string AssignedEmployeeName { get; set; }

        public DateTime DeadLine { get; set; }
    }
}
