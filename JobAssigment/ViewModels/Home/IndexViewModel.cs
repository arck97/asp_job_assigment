﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobAssigment.ViewModels.Home
{
    public enum JobStatus
    {
        Accepted, Declined, Pending
    }

    public class IndexViewModel
    {
        public bool IsAdmin { get; set; }

        public List<IndexListItemViewModel> IndexListItems { get; set; }

        public JobStatus SelectedStatus { get; set; }

        public IndexViewModel()
        {
            IndexListItems = new List<IndexListItemViewModel>();
            SelectedStatus = JobStatus.Pending;
        }
    }
}
