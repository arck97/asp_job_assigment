﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JobAssigment.ViewModels.Job
{
    public class EditViewModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(20)]
        public string JobName { get; set; } //Munka elnevezése

        public string AssignedToEmployeeId { get; set; }

        public DateTime StartTheJob { get; set; } //Amikor kezdödik a munka

        public DateTime EndTheJob { get; set; } //Amikorra bekell fejezni a munkát

        public DateTime DeadLine { get; set; } //Munka lejárati ideje

        public List<SelectListItem> AvailableEmployees { get; set; }


        public EditViewModel()
        {
            AvailableEmployees = new List<SelectListItem>();
        }
    }
}
