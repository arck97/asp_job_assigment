﻿using System;
using System.Collections.Generic;
using System.Text;
using JobAssigment.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JobAssigment.Data
{
    public class ApplicationDbContext : IdentityDbContext<Employee> // már meg van a user-em
    {
        public DbSet<Job> Jobs { get; set; }

        public DbSet<Company> Companies { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    } //add-migration company_jobs
}
