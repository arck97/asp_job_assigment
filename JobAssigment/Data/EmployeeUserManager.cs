﻿using JobAssigment.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobAssigment.Data
{
    public class EmployeeUserManager : UserManager<Employee>
    {
        private readonly ApplicationDbContext _dbContext;

        public EmployeeUserManager(ApplicationDbContext dbContext, IUserStore<Employee> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<Employee> passwordHasher, IEnumerable<IUserValidator<Employee>> userValidators, IEnumerable<IPasswordValidator<Employee>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<Employee>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _dbContext = dbContext;
        }
    }
}
