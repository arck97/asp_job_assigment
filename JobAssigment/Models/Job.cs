﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JobAssigment.Models
{
    public class Job
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(50)]
        [MinLength(5)]
        //[Required]
        public string Name { get; set; } //Munka elnevezése

        public bool? IsAcceptedTheJob { get; set; } //elfogadta-e a munkát, ha null akkor pending

        [Required]
        public string OwnerId { get; set; }

        public virtual Employee Owner { get; set; } // a munka menedzsere

        public string AssignedEmployeeId { get; set; }

        public virtual Employee AssignedEmployee { get; set; } //Aki elfogadta a munkát

        [MaxLength(500)]
        public string Description { get; set; } //leirás

        [Required]
        public DateTime StartTheJob { get; set; } //Amikor kezdödik a munka

        [Required]
        public DateTime EndTheJob { get; set; } //Amikorra bekell fejezni a munkát

        [Required]
        public DateTime DeadLine { get; set; } //Munka lejárati ideje

    }
}
