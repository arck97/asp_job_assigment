﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JobAssigment.Models
{
    public class Employee : IdentityUser
    {
        [Required]
        public string Name { get; set; }

        //[Required]
        public virtual Company Company { get; set; }

    }
}
