﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JobAssigment.Migrations
{
    public partial class ownerId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_AspNetUsers_AssignedEmployeeId",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_AspNetUsers_OwnerId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_AssignedEmployeeId",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_OwnerId",
                table: "Jobs");

            migrationBuilder.AlterColumn<int>(
                name: "OwnerId",
                table: "Jobs",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<int>(
                name: "AssignedEmployeeId",
                table: "Jobs",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AssignedEmployeeId1",
                table: "Jobs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerId1",
                table: "Jobs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_AssignedEmployeeId1",
                table: "Jobs",
                column: "AssignedEmployeeId1");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_OwnerId1",
                table: "Jobs",
                column: "OwnerId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_AspNetUsers_AssignedEmployeeId1",
                table: "Jobs",
                column: "AssignedEmployeeId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_AspNetUsers_OwnerId1",
                table: "Jobs",
                column: "OwnerId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_AspNetUsers_AssignedEmployeeId1",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_AspNetUsers_OwnerId1",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_AssignedEmployeeId1",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_OwnerId1",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "AssignedEmployeeId1",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "OwnerId1",
                table: "Jobs");

            migrationBuilder.AlterColumn<string>(
                name: "OwnerId",
                table: "Jobs",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "AssignedEmployeeId",
                table: "Jobs",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_AssignedEmployeeId",
                table: "Jobs",
                column: "AssignedEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_OwnerId",
                table: "Jobs",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_AspNetUsers_AssignedEmployeeId",
                table: "Jobs",
                column: "AssignedEmployeeId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_AspNetUsers_OwnerId",
                table: "Jobs",
                column: "OwnerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
